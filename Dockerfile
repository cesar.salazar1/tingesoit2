FROM nginx
WORKDIR /frontend
COPY /dist /frontend
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 8080
